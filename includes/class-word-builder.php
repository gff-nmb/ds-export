<?php

declare( strict_types=1 );

namespace DSExport;

use Error;
use Exception;
use PhpOffice\PhpWord\Element\AbstractContainer;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\SimpleType\Jc;
use PhpOffice\PhpWord\Style\Tab;

class Word_Builder {
	/**
	 * @var array<string, array>
	 */
	protected array $languages;

	public function __construct() {
		$default_lang     = apply_filters( 'wpml_default_language', null );
		$active_languages = apply_filters( 'wpml_active_languages', null );

		$languages = [];
		if ( array_key_exists( $default_lang, $active_languages ) ) {
			$languages[ $default_lang ] = $active_languages[ $default_lang ];
			unset( $active_languages[ $default_lang ] );
		}
		foreach ( $active_languages as $lang => $language ) {
			$languages[ $lang ] = $language;
		}

		$this->languages = $languages;
	}

	/**
	 * @param string                                $title
	 * @param int                                   $home_poly
	 * @param array<int, Post_Container_Collection> $post_container_collections
	 *
	 * @return PhpWord
	 */
	public function build( string $title, int $home_poly, array $post_container_collections ): PhpWord {
		Settings::setOutputEscapingEnabled( true );
		Settings::setDefaultFontName( 'Arial' );
		Settings::setDefaultFontSize( 12 );

		$word = new PhpWord();
		$word->setDefaultParagraphStyle(
			[ 'spaceAfter' => Converter::cmToTwip( .5 ), 'lineHeight' => 1.3, ] );
		$title_f_style = $word->addFontStyle( 'Document Heading',
			[ 'size' => 25, 'bold' => true, ],
			[ 'spaceAfter' => Converter::cmToTwip( 2 ), 'alignment' => Jc::CENTER, ] );
		$word->addTitleStyle( 1,
			[ 'size' => 20, ],
			[ 'spaceAfter' => Converter::cmToTwip( .5 ), ] );
		$word->addTitleStyle( 2,
			[ 'size' => 16, ],
			[ 'spaceAfter' => Converter::cmToTwip( .25 ), ] );
		$word->addTitleStyle( 3,
			[ 'size' => 14, ],
			[ 'spaceAfter' => Converter::cmToTwip( .25 ), ] );

		$section = $word->addSection();
		$section->addText( $title, $title_f_style );;
		$section->addTOC( [ 'size' => 11, 'italic' => true, ], [
			'tabLeader' => Tab::TAB_LEADER_NONE,
			'tabPos'    => Converter::cmToTwip( 16 ),
		], 1, 3 );

		foreach ( $this->languages as $lang => $language ) {
			$section->addTitle( $language['native_name'], 1 );

			// Home.
			if ( array_key_exists( $home_poly, $post_container_collections ) ) {
				$pcc = $post_container_collections[ $home_poly ];
				$this->writePostContainer( $section, $pcc->getPostContainer( $lang ) );
			}

			// Other Posts.
			foreach ( $post_container_collections as $poly => $post_container_collection ) {
				if ( $poly === $home_poly ) {
					continue;
				}

				$this->writePostContainer( $section, $post_container_collection->getPostContainer( $lang ) );
			}
		}

		return $word;
	}

	protected function writePostContainer( AbstractContainer $container, ?Post_Container $post_container ): void {
		if ( is_null( $post_container ) ) {
			return;
		}

		$container->addTitle( $post_container->getTitle(), 2 );

		try {
			Html::addHtml( $container, $post_container->getHtml(), false, false );
		} catch ( Exception|Error $e ) {
			$container->addText( 'Exception/Error: ' . $e->getMessage(), [ 'color' => 'ff0000' ] );
		}

		$container->addText( '', null, [ 'spaceAfter' => Converter::cmToTwip( 2 ) ] );
	}
}
