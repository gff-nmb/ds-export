<?php

declare( strict_types=1 );

namespace DSExport;

class Post_Container {
	protected string $title;
	protected string $html;

	public function __construct( string $title, string $html ) {
		$this->title = $title;
		$this->html  = $html;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getHtml(): string {
		return $this->html;
	}
}
