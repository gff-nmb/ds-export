<?php

declare( strict_types=1 );

namespace DSExport;

class Post_Container_Collection {
	protected int $poly;

	/**
	 * @var array<string, Post_Container>
	 */
	protected array $post_containers;

	public function __construct( int $poly ) {
		$this->poly = $poly;
	}

	public function setPostContainer( string $lang, Post_Container $post_container ) {
		$this->post_containers[ $lang ] = $post_container;
	}

	public function getPostContainer( string $lang ):?Post_Container {
		if ( array_key_exists($lang, $this->post_containers)) {
			return $this->post_containers[$lang];
		}

		return null;
	}
}
