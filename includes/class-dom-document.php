<?php

declare( strict_types=1 );

namespace DSExport;

use DOMAttr;
use DOMDocument;
use DOMNamedNodeMap;
use DOMNode;
use DOMNodeList;
use DOMXPath;

class Dom_Document extends DOMDocument {
	protected ?DOMXPath $xpath = null;

	/**
	 * @param string       $expression
	 * @param DOMNode|null $contextNode
	 * @param bool         $registerNodeNS
	 *
	 * @return DOMNodeList<DOMNode>|null
	 */
	public function xpath(
		string $expression,
		?DOMNode $contextNode = null,
		bool $registerNodeNS = true
	): ?DOMNodeList {
		if ( is_null( $this->xpath ) ) {
			$this->xpath = new DOMXPath( $this );
		}

		$nodes = $this->xpath->query( $expression, $contextNode, $registerNodeNS );
		if ( $nodes instanceof DOMNodeList ) {
			return $nodes;
		}

		return null;
	}

	public function hasAttribute( DOMNode $node, string $attribute_name ): bool {
		if ( $node->attributes instanceof DOMNamedNodeMap ) {
			$attr = $node->attributes->getNamedItem( $attribute_name );
			if ( $attr instanceof DOMAttr ) {
				return true;
			}
		}

		return false;
	}

	public function getAttributeValue( DOMNode $node, string $attribute_name ): ?string {
		if ( $node->attributes instanceof DOMNamedNodeMap ) {
			$attr = $node->attributes->getNamedItem( $attribute_name );
			if ( $attr instanceof DOMAttr ) {
				return $attr->value;
			}
		}

		return null;
	}
}
