<?php

declare( strict_types=1 );

namespace DSExport;

use DOMElement;
use DOMNode;
use InvalidArgumentException;
use PhpOffice\PhpWord\Shared\Converter;

class DS_Exporter {
	protected Dom_Document $dom;

	/**
	 * @var array<int, Post_Container_Collection>
	 */
	protected array $post_container_collections = [];

	/**
	 * @var int|null
	 */
	protected ?int $home_poly;

	public function __construct( string $html ) {
		libxml_clear_errors();
		libxml_use_internal_errors( true );

		$this->dom = new Dom_Document();
		if ( ! $this->dom->loadHTML( $html ) ) {
			throw new InvalidArgumentException( 'Html code could not be dom loaded.' );
		}

		$this->parse();
	}

	protected function parse(): void {
		$this->home_poly = null;
		foreach ( $this->dom->xpath( '//div[contains(concat(" ",@class," "), " post-container ")]' ) as $post_container_node ) {
			/** @var DOMElement $post_container_node */
			$lang = $this->dom->getAttributeValue( $post_container_node, 'data-lang' );
			$poly = intval( $this->dom->getAttributeValue( $post_container_node, 'data-poly' ) );

			if ( $this->dom->hasAttribute( $post_container_node, 'data-home' ) ) {
				$this->home_poly = $poly;
			}

			if ( ! array_key_exists( $poly, $this->post_container_collections ) ) {
				$this->post_container_collections[ $poly ] = new Post_Container_Collection( $poly );
			}

			$this->post_container_collections[ $poly ]->setPostContainer(
				$lang,
				new Post_Container(
					$this->parsePostContainerTitle( $post_container_node ),
					$this->transformHtml( $post_container_node )
				)
			);
		}
	}

	protected function parsePostContainerTitle( DOMElement $post_container_node ): string {
		foreach ( $this->dom->xpath( './article', $post_container_node ) as $article_node ) {
			return $this->dom->getAttributeValue( $article_node, 'data-title' ) ?? '';
		}

		return 'Title not found.';
	}

	protected function transformHtml( DOMElement $post_container_node ): string {
		foreach ( $this->dom->xpath( './/a[@href]', $post_container_node ) as $a_node ) {
			/** @var DOMElement $a_node */
			if ( $this->dom->xpath( './img', $a_node )->count() > 0 ) {
				// As this a-node has an img-node as a direct child, we need to remove the href in order to be able to export the image.
				/** @var DOMNode|null $parent */
				$parent = $a_node->parentNode;
				if ( ! is_null( $parent ) ) {
					$new_node = $this->dom->createElement( 'div' );
					foreach ( $a_node->childNodes as $child ) {
						$new_node->appendChild( $child );
					}

					$parent->replaceChild( $new_node, $a_node );
				}
			}
		}

		foreach ( $this->dom->xpath( './/img', $post_container_node ) as $img_node ) {
			/** @var DOMElement $img_node */
			$src = $this->dom->getAttributeValue( $img_node, 'src' );
			if ( is_null( $src ) ) {
				// Remove this `img` node as the `src` attribute is missing.
				$parent = $img_node->parentNode;
				if ( $parent instanceof DOMNode ) {
					$parent->removeChild( $img_node );
				}

				continue;
			}

			if ( strcasecmp( pathinfo( $src, PATHINFO_EXTENSION ), 'svg' ) === 0 ) {
				$parent = $img_node->parentNode;
				if ( $parent instanceof DOMNode ) {
					$node = $this->dom->createElement( 'p' );
					$node->setAttribute( 'style', 'color:#ff0000;' );
					$node->appendChild( $this->dom->createTextNode( 'We are sorry, but svg images cannot be exported. Therefore we have removed this image.' ) );

					$parent->replaceChild( $node, $img_node );
				}

				continue;
			}

			$current_width  = $this->dom->getAttributeValue( $img_node, 'width' );
			$current_height = $this->dom->getAttributeValue( $img_node, 'height' );
			$new_width      = Converter::cmToPixel( 5 );

			if ( ! empty( $current_width ) && ! empty( $current_height ) ) {
				$img_node->setAttribute( 'width', sprintf( '%d', $new_width ) );
				$img_node->setAttribute( 'height', sprintf( '%d', $current_height * $new_width / $current_width ) );
			}
		}

		return $post_container_node->C14N();
	}

	/**
	 * @return array<string, Post_Container_Collection>
	 */
	public function getPostContainerCollections(): array {
		return $this->post_container_collections;
	}

	/**
	 * @return int|null
	 */
	public function getHomePoly(): ?int {
		return $this->home_poly;
	}
}
