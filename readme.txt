=== DS Export ===
Contributors: masterida
Donate link: https://www.gff.ch/
Requires at least: 5.8
Tested up to: 6.1
Requires PHP: 7.4
Stable tag: 1.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The DS Export plugin.

== Description ==

The **DS Export** plugin.

== Changelog ==

= 1.1 =
* Add admin page.
* Set required capability to `manage_categories`.

= 1.0 =
* Initial version.
