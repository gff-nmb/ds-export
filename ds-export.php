<?php

/**
 * Plugin Name:       DS Export
 * Description:       The amazing exporter for Digital Signage Screens.
 * Requires at least: 5.8
 * Requires PHP:      7.4
 * Version:           1.1
 * Author:            Adrian Suter
 * Author URI:        https://www.gff.ch/
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       ds-export
 */

declare( strict_types=1 );

use DSExport\DS_Exporter;
use DSExport\Word_Builder;
use PhpOffice\PhpWord\IOFactory;
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;

/**
 * Plugin autoloader.
 */
( include( __DIR__ . '/autoload.php' ) )( 'DSExport', __DIR__ . '/includes' );

/**
 * Absolute filesystem path to the `ds-export` plugin root file.
 */
define( 'DS_EXPORT_ABS_PLUGIN_FILE', __FILE__ );

/**
 * Text Domain.
 */
define( 'DS_EXPORT_DOMAIN', 'ds-export' );

/**
 * Add `plugins_loaded` handler.
 */
add_action( 'plugins_loaded', function (): void {
	load_plugin_textdomain( DS_EXPORT_DOMAIN, false, 'ds-export/locales' );

	add_action( 'admin_menu', function (): void {
		// toplevel_page_ds-export-admin-dashboard
		add_menu_page(
			__( 'Export', DS_EXPORT_DOMAIN ),
			__( 'Export', DS_EXPORT_DOMAIN ),
			'manage_categories',
			'ds-export-admin-dashboard',
			'ds_export_admin_dashboard_callback',
			'dashicons-text-page',
			1000
		);
	} );
} );

add_action( 'load-toplevel_page_ds-export-admin-dashboard', function () {
	if ( filter_has_var( INPUT_GET, 'export' ) ) {
		$category_slug = filter_input( INPUT_GET, 'export' );
		if ( is_string( $category_slug ) ) {
			$category = get_category_by_slug( $category_slug );
			if ( $category instanceof WP_Term ) {
				// Get the html code for this category page.
				$response = wp_remote_get( get_category_link( $category ) );

				// Instantiate the exporter.
				$exporter = new DS_Exporter( $response['body'] );

				// Instantiate the word builder.
				$word_builder = new Word_Builder();

				// Build the word.
				$word = $word_builder->build(
					'Screen "' . $category->name . '"',
					$exporter->getHomePoly() ?? 0,
					$exporter->getPostContainerCollections()
				);

				header( "Content-Description: File Transfer" );
				header( 'Content-Disposition: inline; filename="' . date( 'Y-m-d' ) . ' Export ' . $category->name . '.docx"' );
				header( 'Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document' );
				header( 'Content-Transfer-Encoding: binary' );
				header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
				header( 'Expires: 0' );

				$writer = IOFactory::createWriter( $word, 'Word2007' );
				$writer->save( 'php://output' );

				exit();
			}
		}
	}
} );

function ds_export_admin_dashboard_callback(): void {
	?>
    <div class="wrap">
        <h1><?php
			_e( 'Export', DS_EXPORT_DOMAIN );
			?></h1>

		<?php
		$categories = [];

		foreach ( get_categories() as $category ) {
			/** @var WP_Term $category */
			$categories[ $category->slug ] = $category->name;
		}

		asort( $categories, SORT_NATURAL );
		foreach ( $categories as $slug => $name ) {
			echo '<p><a href="' . self_admin_url() . '?' . http_build_query( [
					'page'   => 'ds-export-admin-dashboard',
					'export' => $slug,
				] ) . '">' . $name . '</a></p>';
		}
		?>
    </div>
	<?php
}

///////////////////////////// PLUGIN UPDATE CHECKER ////////////////////////////

add_action( 'plugins_loaded', function (): void {
	$group_name  = 'gff-nmb';
	$plugin_name = basename( DS_EXPORT_ABS_PLUGIN_FILE, '.php' );
	if ( class_exists( PucFactory::class ) ) {
		$update_checker = PucFactory::buildUpdateChecker(
			'https://gitlab.com/' . $group_name . '/' . $plugin_name . '/',
			DS_EXPORT_ABS_PLUGIN_FILE
		);

		$vcs_api = $update_checker->getVcsApi();
		if ( method_exists( $vcs_api, 'enableReleasePackages' ) ) {
			$vcs_api->enableReleasePackages();
		} else {
			trigger_error(
				'PUC Updater can not update as the VCS API does not support release packages.',
				E_USER_ERROR
			);
		}
	}
} );
