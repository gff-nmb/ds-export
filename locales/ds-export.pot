# Copyright (C) 2022 A. Suter
# This file is distributed under the same license as the ds-blocks package.
msgid ""
msgstr ""
"Project-Id-Version: ds-export 1.0\n"
"Report-Msgid-Bugs-To: adrian.suter@gff.ch\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

# Plugin Description
msgid "The amazing exporter for Digital Signage Screens."
msgstr ""
